import {makeStyles} from "@mui/styles";
import {Button, InputAdornment, TextField, Typography, useTheme} from "@mui/material";
import {Email, LockOpen, Note, Person, Security, VpnKey} from "@mui/icons-material";
import adImg from "../assets/card.svg"
import {Link, useLocation} from "react-router-dom";

const authStyles = makeStyles((theme) => ({
    container: {
        /* You can also use media queries like this: ['@media (min-width:700px)'] */
        [theme.breakpoints.up('md')]: {
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
        },
        '& > div': {
            display: 'grid',
            minHeight: '100vh',
            boxSizing: 'border-box'
        }
    },
    auth: {
        alignContent: 'start',
        justifyContent: 'center',
        paddingTop: '24vh'
    },
    tabsContainer: {
        display: 'flex',
        alignItems: 'center',
        gap: '20px'
    },
    tabOn: {
        color: theme.palette.primary.main,
        cursor: 'pointer',
        fontWeight: 'bold !important',
        fontSize: '19px !important'
    },
    tabOff: {
        cursor: 'pointer',
        color: '#757575'
    },
    fieldsContainer: {
        display: 'flex',
        flexDirection: 'column',
        rowGap: '20px',
        width: '300px',
        padding: '60px 50px'
    },
    [theme.breakpoints.down('sm')]: {
        fieldsContainer: {
            width: '220px',
        }
    },
    confirmContainer: {
        marginLeft: 'auto'
    },
    ad:{
        background: theme.palette.primary.main,
        padding: '35px',
        alignContent: 'space-between',
    },
    logoContainer: {
        display: 'flex',
        alignItems: 'center',
        gap: '20px',
        color: '#fff'
    },
    logoText: {
        fontWeight: 'normal !important'
    },
    adImg: {
        marginLeft: '15%',
        width: '70%'
    },
    adDescription: {
        color: '#fff',
        padding: '35px',
        fontWeight: 'bold !important',
        paddingBottom: '150px'
    }
}))

const inputProps = {
    login: {
        endAdornment: (
            <InputAdornment position="start">
                <Person />
            </InputAdornment>
        ),
    },
    password: {
        endAdornment: (
            <InputAdornment position="start">
                <Security />
            </InputAdornment>
        ),
    },
    email: {
        endAdornment: (
            <InputAdornment position="start">
                <Email />
            </InputAdornment>
        ),
    }
}

const AuthView = () => {
    const theme = useTheme()
    const styles = authStyles(theme)
    const location = useLocation()
    return(
        <div className={styles.container}>
            <div className={styles.ad}>
                <div className={styles.logoContainer}>
                    <Note fontSize='large'/>
                    <Typography className={styles.logoText} variant='h6'>Flashcards</Typography>
                </div>
                <img className={styles.adImg} src={adImg}/>
                <Typography className={styles.adDescription} variant='h5'>
                    Intelligent language learning platform based on flashcards and user's response statistics
                </Typography>
            </div>
            <div className={styles.auth}>
                <div className={styles.tabsContainer}>
                    <Link to='/login' style={{textDecoration: 'none'}}>
                        <Typography className={location.pathname === '/login' ? styles.tabOn : styles.tabOff}>LOG IN</Typography>
                    </Link>
                    <Link to='/signup' style={{textDecoration: 'none'}}>
                        <Typography className={location.pathname === '/login' ? styles.tabOff : styles.tabOn}>SIGN UP</Typography>
                    </Link>
                </div>
                <div className={styles.fieldsContainer}>
                    <TextField size='small' label="Login" variant="outlined" InputProps={inputProps.login}/>
                    <TextField size='small' label="Password" variant="outlined" InputProps={inputProps.password}/>
                    {location.pathname === '/signup' ?
                        <>
                            <TextField size='small' label="Password repeate" variant="outlined" InputProps={inputProps.password}/>
                            {/*Maybe email field for 'password forget' and 'email verification' functionalities in the future (not necessary but useful)*/}
                            {/*<TextField size='small' label="Email" variant="outlined" InputProps={inputProps.email}/>*/}
                        </> : ''}
                </div>
                <div className={styles.confirmContainer}>
                    {location.pathname === '/login' ?
                        <Button variant="contained" startIcon={<LockOpen/>}>Log In</Button> :
                        <Button variant="contained" startIcon={<VpnKey/>}>Sign Up</Button>
                    }
                </div>
            </div>
        </div>
    )
}

export default AuthView