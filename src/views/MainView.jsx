import {
    AppBar, BottomNavigation, BottomNavigationAction, Divider,
    Drawer,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText, ThemeProvider,
    Toolbar,
    Typography,
    useTheme
} from "@mui/material";
import {
    AccountCircle,
    Note,
    Menu,
    Inbox,
    Mail,
    Explore,
    AddCircleOutline,
    Create,
    School,
    MusicNote, Movie, Settings, PowerSettingsNew, Restore, Favorite, LocationOn
} from "@mui/icons-material";
import {makeStyles} from "@mui/styles";
import {BrowserRouter as Router, Redirect, Route} from "react-router-dom";
import AuthView from "./AuthView";
import ExploreGroupsView from "./ExploreGroupsView";
import NewGroupView from "./NewGroupView";
import YourGroupsView from "./YourGroupsVeiw";
import YourCoursesView from "./YourCoursesView";
import BbcListeningView from "./BbcListeningView";
import TedMoviesView from "./TedMoviesView";
import SettingsView from "./SettingsView";
import { Link } from "react-router-dom";
import {useState} from "react";

const authStyles = makeStyles((theme) => ({
    barContainer: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    logoContainer: {
        display: 'flex',
        alignItems: 'center',
        gap: '10px',
        [theme.breakpoints.down('md')]: {
            display: 'none'
        }
    },
    usernameContainer: {
        display: 'flex',
        alignItems: 'center',
        gap: '10px',
    },
    workArea: {
        display: 'grid',
        gridTemplateColumns: '240px auto',
        [theme.breakpoints.down('md')]: {
            gridTemplateColumns: '0px auto',
        }
    },
    content: {
        padding: '25px'
    }
}))

const drawerWidth = 240;

const drawerMain = [
    {
        icon: <Explore/>,
        label: 'Explore groups',
        link: '/dashboard/explore-groups'
    },
    {
        icon: <Create/>,
        label: 'New group',
        link: '/dashboard/new-group'
    },
    {
        icon: <Note/>,
        label: 'Your groups',
        link: '/dashboard/your-groups'
    }
]

const drawerOther = [
    {
        icon: <Settings/>,
        label: 'Settings',
        link: '/dashboard/settings'
    },
    {
        icon: <PowerSettingsNew/>,
        label: 'Logout',
        link: '/login'
    }
]

const drawer = (
  <>
      <Toolbar/>
      <List>
          <ListItem button component={Link} to='/dashboard/your-courses' key='Your courses'>
              <ListItemIcon>
                  <School/>
              </ListItemIcon>
              <ListItemText primary='Your courses' />
          </ListItem>
      </List>
      <Divider/>
      <List>
          {drawerMain.map((item, index) => (
              <ListItem button component={Link} to={item.link} key={item.label}>
                  <ListItemIcon>
                      {item.icon}
                  </ListItemIcon>
                  <ListItemText primary={item.label} />
              </ListItem>
          ))}
      </List>
      <Divider/>
      <List>
          {drawerOther.map((item, index) => (
              <ListItem button component={Link} to={item.link} key={item.label}>
                  <ListItemIcon>
                      {item.icon}
                  </ListItemIcon>
                  <ListItemText primary={item.label} />
              </ListItem>
          ))}
      </List>
  </>
)

const MainView = () => {
    const theme = useTheme()
    const styles = authStyles(theme)
    const [mobileOpen, setMobileOpen] = useState(false)
    const [value, setValue] = useState(0);
    return(
        <>
            <AppBar position="fixed" sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}>
                <Toolbar>
                    <div className={styles.barContainer}>
                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            sx={{ mr: 2, display: {sm: 'block', md: 'none'}}}
                            onClick={() => setMobileOpen(!mobileOpen)}
                        >
                            <Menu/>
                        </IconButton>
                        <div className={styles.logoContainer}>
                            <Note/>
                            <Typography variant='body1'>Flashcards</Typography>
                        </div>
                        <Typography variant='h6'>Your courses</Typography>
                        <div className={styles.usernameContainer}>
                            <Typography sx={{display: {xs: 'none', sm: 'none', md: 'block'}}} variant='body1'>Username</Typography>
                            <AccountCircle fontSize='large'/>
                        </div>
                    </div>
                </Toolbar>
            </AppBar>
            <div className={styles.workArea}>
                <div className={styles.drawer}>
                    <Drawer variant="permanent" sx={{
                        display: {xs: 'none', sm: 'none', md: 'block'},
                        [`& .MuiDrawer-paper`]: { width: drawerWidth, boxSizing: 'border-box' },
                    }}>
                        {drawer}
                    </Drawer>
                    <Drawer
                        variant="temporary"
                        open={mobileOpen}
                        onClose={() => setMobileOpen(!mobileOpen)}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                        sx={{
                            display: { sm: 'block', md: 'none' },
                            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                        }}
                    >
                        {drawer}
                    </Drawer>
                </div>
                <div className={styles.content}>
                    <Toolbar/>
                    <Route exact path='/dashboard/explore-groups' component={ExploreGroupsView}/>
                    <Route exact path='/dashboard/new-group' component={NewGroupView}/>
                    <Route exact path='/dashboard/your-groups' component={YourGroupsView}/>
                    <Route exact path='/dashboard/your-courses' component={YourCoursesView}/>
                    <Route exact path='/dashboard/bbc-listening' component={BbcListeningView}/>
                    <Route exact path='/dashboard/ted-movies' component={TedMoviesView}/>
                    <Route exact path='/dashboard/settings' component={SettingsView}/>
                </div>
                <BottomNavigation
                    showLabels
                    value={value}
                    onChange={(event, newValue) => {
                        setValue(newValue);
                    }}
                >
                    <BottomNavigationAction label="Recents" icon={<Restore/>} />
                    <BottomNavigationAction label="Favorites" icon={<Favorite/>} />
                    <BottomNavigationAction label="Nearby" icon={<LocationOn/>} />
                </BottomNavigation>
            </div>
        </>
    )
}

export default MainView