import {Grid} from "@mui/material";
import * as React from "react";
import GroupCard from "../components/GroupCard";
import CardFilter from "../components/CardFilter";

const ExploreGroupsView = () => {
    return(
        <Grid container justifyContent="center">
            <Grid item container spacing={3} sx={{ maxWidth: '1920px' }}>
                <CardFilter/>
                <GroupCard/>
                <GroupCard/>
                <GroupCard/>
                <GroupCard/>
                <GroupCard/>
                <GroupCard/>
                <GroupCard/>
                <GroupCard/>
                <GroupCard/>
                <GroupCard/>
            </Grid>
        </Grid>
    )
}

export default ExploreGroupsView