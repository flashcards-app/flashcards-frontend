import {Grid} from "@mui/material";
import * as React from "react";
import GroupCard from "../components/GroupCard";
import CardFilter from "../components/CardFilter";

const YourGroupsView = () => {
    return(
        <Grid container spacing={3}>
            <CardFilter/>
            <GroupCard modifiable={true}/>
            <GroupCard modifiable={true}/>
            <GroupCard modifiable={true}/>
            <GroupCard modifiable={true}/>
            <GroupCard modifiable={true}/>
            <GroupCard modifiable={true}/>
            <GroupCard modifiable={true}/>
        </Grid>
    )
}

export default YourGroupsView