import {
    Button,
    Card,
    CardActionArea,
    CardActions,
    CardContent,
    Grid,
    LinearProgress,
    Typography,
    useTheme
} from "@mui/material";
import {makeStyles} from "@mui/styles";

const card = (
    <Grid item xs={12} sm={6} md={6} lg={4}>
        <Card>
            <CardActionArea>
                <CardContent>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <Typography variant='subtitle2' color='primary.main' sx={{fontSize: '20px'}}>
                            Instruments
                        </Typography>
                        <div style={{display: 'flex'}}>
                            <Typography variant='subtitle1' color='text.secondary' sx={{fontSize: '18px'}}>
                                Level:
                            </Typography>
                            <Typography variant='subtitle2' color='primary.main' sx={{fontSize: '20px'}}>
                                B2
                            </Typography>
                        </div>
                    </div>
                </CardContent>
                <CardContent>
                    <CardContent>
                        <Typography variant='h6' align='center'>
                            B2 Instruments 1st try
                        </Typography>
                        <LinearProgress variant='determinate' value={60} />
                        <Typography variant='body2' align='center' color='text.secondary'>
                            Progress
                        </Typography>
                    </CardContent>
                </CardContent>
                <CardContent>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <Typography variant='subtitle1' color='text.secondary'>
                            From: Polish
                        </Typography>
                        <Typography variant='subtitle1' color='text.secondary'>
                            To: English
                        </Typography>
                    </div>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button>Restart</Button>
                <Button color='error'>Delete</Button>
            </CardActions>
        </Card>
    </Grid>
)

const YourCoursesView = () => {
    return(
        <Grid container spacing={3}>
            {card}
            {card}
            {card}
            {card}
            {card}
            {card}
            {card}
            {card}
            {card}
            {card}
            {card}
        </Grid>
    )
}

export default YourCoursesView