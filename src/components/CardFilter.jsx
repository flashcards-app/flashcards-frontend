import {Button, Chip, Grid, Stack, TextField} from "@mui/material";
import {FilterList} from "@mui/icons-material";
import * as React from "react";

const CardFilter = () => {
    return(
        <>
            <Grid item xs={12} sm={9} lg={10}>
                <TextField fullWidth label="Explore groups"/>
            </Grid>
            <Grid container item xs={12} sm={3} lg={2}>
                <Button variant="contained" endIcon={<FilterList />} fullWidth>
                    filters
                </Button>
            </Grid>
            <Grid container item xs={12} justifyContent="center">
                <Stack direction="row" spacing={1}>
                    <Chip
                        label="Deletable"
                        onDelete={()=>{}}
                    />
                    <Chip
                        label="Deletable"
                        onDelete={()=>{}}
                    />
                </Stack>
            </Grid>
        </>
    )
}

export default CardFilter