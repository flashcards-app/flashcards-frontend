import {Button, Card, CardActionArea, CardActions, CardContent, Grid, Stack, Switch, Typography} from "@mui/material";
import * as React from "react";
import WordListDialog from "./WordListDialog";
import {useState} from "react";

const GroupCard = ({modifiable=false}) => {
    const [open, setOpen] = React.useState(false);
    const [checked, setChecked] = useState(true);

    const handleChange = (event) => {
        setChecked(event.target.checked);
    };

    return (
        <>
            <WordListDialog open={open} setOpen={setOpen} modifiable={modifiable}/>
            <Grid item xs={12} sm={6} md={6} lg={4}>
                <Card sx={{ minWidth: 275 }}>
                    <CardActionArea onClick={()=>setOpen(true)}>
                        <CardContent>
                            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                <Typography variant='subtitle2' color='primary.main' sx={{fontSize: '20px'}}>
                                    Instruments
                                </Typography>
                                <div style={{display: 'flex'}}>
                                    <Typography variant='subtitle1' color='text.secondary' sx={{fontSize: '18px'}}>
                                        Level:
                                    </Typography>
                                    <Typography variant='subtitle2' color='primary.main' sx={{fontSize: '20px'}}>
                                        B2
                                    </Typography>
                                </div>
                            </div>
                        </CardContent>
                        <CardContent>
                            <CardContent>
                                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                    <Typography variant="subtitle2">
                                        Size (words):
                                    </Typography>
                                    <Typography variant="body1">
                                        725
                                    </Typography>
                                </div>
                                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                    <Typography variant="subtitle2">
                                        Created:
                                    </Typography>
                                    <Typography variant="body1">
                                        04.12.2021r.
                                    </Typography>
                                </div>
                                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                    <Typography variant="subtitle2">
                                        Modified:
                                    </Typography>
                                    <Typography variant="body1">
                                        04.12.2021r.
                                    </Typography>
                                </div>
                                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                    <Typography variant="subtitle2">
                                        Author:
                                    </Typography>
                                    <Typography variant="body1">
                                        NorbertBaran
                                    </Typography>
                                </div>
                            </CardContent>
                            <Typography variant="body2" color='primary.main' sx={{textAlign: 'center'}}>
                                Polish, English
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    {modifiable ? (
                        <CardContent>
                            <Stack direction='row' justifyContent='space-between'>
                                <Stack direction="row" spacing={1} alignItems='center'>
                                    <Typography variant='subtitle2' color='primary.main'>
                                        Privacy:
                                    </Typography>
                                    <Switch
                                        checked={checked}
                                        onChange={handleChange}
                                    />
                                </Stack>
                                <Button color='error'>Delete</Button>
                            </Stack>
                        </CardContent>
                    ) : (<></>)}
                </Card>
            </Grid>
        </>
    )
}

export default GroupCard