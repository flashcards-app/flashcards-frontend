import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function createData(English, Polish, German) {
    return {English, Polish, German};
}

const rows = [
    createData('Car', 'Samochód', 'Auto'),
    createData('Bike', 'Rower', 'Fahrrad'),
    createData('Phone', 'Telefon', 'Fernsprecher')
];

export default function WordList({modifiable = false}) {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>English</TableCell>
                        <TableCell>Polish</TableCell>
                        <TableCell>German</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow>
                            <TableCell>{row.English}</TableCell>
                            <TableCell>{row.Polish}</TableCell>
                            <TableCell>{row.German}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
