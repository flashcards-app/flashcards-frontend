import * as React from 'react';
import Button from '@mui/material/Button';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import WordList from "./WordList";
import {Box, Chip, Grid, Stack, TextField} from "@mui/material";
import {Add, Close, Delete, Download, FilterList, Update} from "@mui/icons-material";

export default function WordListDialog({open, setOpen, modifiable=false}) {
    return (
        <Dialog maxWidth="xl" onClose={()=>setOpen(false)} open={open} sx={{padding: '25px'}}>
            <Box p={4}>
                <Grid container spacing={2}>
                    <Grid container justifyContent='space-between' item xs={12}>
                        <Typography variant='subtitle2' color='primary.main' sx={{fontSize: '20px'}}>
                            Instruments
                        </Typography>
                        <Stack direction="row" spacing={1}>
                            <Typography variant='subtitle1' color='text.secondary' sx={{fontSize: '18px'}} align='right'>
                                Level:
                            </Typography>
                            <Typography variant='subtitle2' color='primary.main' sx={{fontSize: '20px'}}>
                                B2
                            </Typography>
                        </Stack>
                    </Grid>
                    <Grid container spacing={1} justifyContent='center' item>
                        <Grid item>
                            <Chip
                                label="Polish"
                            />
                        </Grid>
                        <Grid item>
                            <Chip
                                label="English"
                            />
                        </Grid>
                    </Grid>
                    <Grid container item>
                        <WordList/>
                    </Grid>
                    <Grid container spacing={4} justifyContent='center' item>
                        {modifiable ? (
                            <>
                                <Grid container item xs={12} spacing={1} justifyContent='center'>
                                    <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                        <TextField label="English" variant="outlined" size='small' fullWidth/>
                                    </Grid>
                                    <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                        <TextField label="Polish" variant="outlined" size='small' fullWidth/>
                                    </Grid>
                                    <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                        <TextField label="German" variant="outlined" size='small' fullWidth/>
                                    </Grid>
                                    <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                        <Button variant='contained' endIcon={<Add />} fullWidth>
                                            Add word
                                        </Button>
                                    </Grid>
                                </Grid>
                                <Grid container item xs={12} spacing={1} justifyContent='center'>
                                    <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                        <Button variant="outlined" endIcon={<Add />} fullWidth>
                                            Add from csv
                                        </Button>
                                    </Grid>
                                    <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                        <Button variant="outlined" endIcon={<Update />} fullWidth>
                                            Update from csv
                                        </Button>
                                    </Grid>
                                    <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                        <Button variant="outlined" color='error' endIcon={<Delete />} fullWidth>
                                            Delete from csv
                                        </Button>
                                    </Grid>
                                    <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                        <Button variant="outlined" endIcon={<Download />} fullWidth>
                                            Download to csv
                                        </Button>
                                    </Grid>
                                    <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                        <Button variant="contained" endIcon={<Close />} fullWidth>
                                            Close
                                        </Button>
                                    </Grid>
                                </Grid>
                            </>
                        ) : (<>
                            <Grid item xs={12} sx={{ maxWidth: {sm: '210px'}}}>
                                <Button variant="contained" endIcon={<Close />} fullWidth>
                                    Close
                                </Button>
                            </Grid>
                        </>)}
                    </Grid>
                </Grid>
            </Box>
        </Dialog>
    );
}