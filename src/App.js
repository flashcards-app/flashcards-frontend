import AuthView from "./views/AuthView";
import {green} from "@mui/material/colors";
import {createTheme, ThemeProvider} from "@mui/material";
import {BrowserRouter as Router, Route, Redirect} from "react-router-dom";
import MainView from "./views/MainView";
import {useState} from "react";

const theme = createTheme({
    palette: {
        primary: {
            main: '#1F345F'
        },
        secondary: {
            main: green[500]
        }
    }
})

const App = () => {
    const [loggedIn, setLoggedIn] = useState(true)
    return(
        <ThemeProvider theme={theme}>
            <Router>
                <Route path='/dashboard' render={()=>(loggedIn ? <MainView/> : <Redirect to='/login'/>)}/>
                <Route exact path='/login' render={()=>(!loggedIn ? <AuthView/> : <AuthView/>)}/>
                <Route exact path='/signup' render={()=>(!loggedIn ? <AuthView/> : <Redirect to='/dashboard'/>)}/>
            </Router>
        </ThemeProvider>
    )
}

export default App